port=8889
# It's OK to put ~ in the dir name
user_file_dir="~/tmp/speechfiles"
# Note that the file locations that are given to speech2text must be relative to the Docker container
# The {audio} and {trs} placeholders are replaced
transcribe_command="docker exec -t speech2text /opt/kaldi-offline-transcriber/speech2text.sh --trs /opt/speechfiles/{trs} /opt/speechfiles/{audio}"
