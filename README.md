## Dependencies

  * Python 3.5 or later

## Installation

Clone this git repository:

    git clone <repo>
    
Change to the directory:

    cd kaldi-offline-transcriber-web
    
Install required Python packages. The pip command should correspond to Python 3 pip.

    pip install --no-cache-dir -r requirements.txt
    
Open `config.py` and change the port to appropritae value. Also the `user_file_dir` 
property should point to the same directory that is mapped to Docker container speech2text's
/opt/speechfiles directory. You don't have to change the `transcribe_command` property
if you run speech2text 'normally'.

## Running 

To run the server (`python` should point to Python 3.5 or later):

    python main.py --config=config.py
    
## Transcribing a file

(In another terminal), make a PUT request to the server, e.g.:

    curl -s -v -T sample/intervjuu2018101605_trimmed.mp3 'http://localhost:8889/upload?callback=http://localhost:8890/notify&extension=mp3'
        
If the audio file exists, the server will respond with HTTP status 200 (i.e., successful):

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.3
    Content-Type: text/html; charset=UTF-8
    Date: Tue, 16 Oct 2018 10:35:16 GMT
    X-Request-Id: ea76c985-51d2-41be-b9b7-4e4d739c01ae
    Content-Length: 0

Note that one of the response headers is `X-Request-Id`, which specifies the
request ID for the upload. This could be useful later.

The server should print something like that to output:

    [I 181016 13:35:16 web:2064] 200 PUT /upload?callback=http://localhost:8890/notify&extension=mp3 (::1) 27.96ms
    [I 181016 13:35:16 main:40] Starting to transcribe file
    [I 181016 13:35:16 main:43] Running command: docker exec -it speech2text /opt/kaldi-offline-transcriber/speech2text.sh --trs /opt/speechfiles/ea76c985-51d2-41be-b9b7-4e4d739c01ae.trs /opt/speechfiles/ea76c985-51d2-41be-b9b7-4e4d739c01ae.mp3
    
When the transcription process finishes, the server will make a callback PUT request to the URL specified in the upload request (http://localhost:8890/notify),
with an id=<X-request-Id> URL parameter, e.g. `http://localhost:8890/notify?id=ea76c985-51d2-41be-b9b7-4e4d739c01ae`,
and the transcription of the audio is in the request body:

    PUT /notify?id=ea76c985-51d2-41be-b9b7-4e4d739c01ae HTTP/1.1
    X-Status: 0
    X-Status-Reason: OK
    Content-Type: application/xml
    Connection: close
    Host: localhost:8890
    Content-Length: 1245
    Accept-Encoding: gzip

    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE Trans SYSTEM "trans-14.dtd">
    <Trans scribe="est-speech2txt" audio_filename="441c9090-0364-4a3d-9d40-1f66da92a4c2" version="1" version_date="181016">
    <Speakers>
    ....

If the trancription process failed, the callback PUT request will contain the processing log in the body, and
the header `X-Status` will be non-zero:

    PUT /notify?id=3629cef3-77b0-4057-b7a2-56949b083d15 HTTP/1.1
    X-Status: 1
    X-Status-Reason: Transcribing failed
    Connection: close
    Host: localhost:8890
    Content-Length: 2863
    Accept-Encoding: gzip

    /opt/kaldi-offline-transcriber/speech2text.sh --trs /opt/speechfiles/3629cef3-77b0-4057-b7a2-56949b083d15.trs /opt/speechfiles/3629cef3-77b0-4057-b7a2-56949b083d15.mp3
    mkdir -p `dirname build/audio/base/3629cef3-77b0-4057-b7a2-56949b083d15.wav`
    ....


You can also retrieve the transcription of the uploaded file any time, by making a GET request to `/result?id=<X-request-Id>`, e.g.
to `http://localhost:8889/result?id=ea76c985-51d2-41be-b9b7-4e4d739c01ae`

If trancription process hasn't finished yet, the server will respond with status 204:

    HTTP/1.1 204 In progress
    Server: TornadoServer/4.5.3
    Date: Tue, 16 Oct 2018 11:49:12 GMT

If the transcription has finished (and was successful), the transcription will be in the response body:

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.3
    Content-Type: application/xml
    Date: Tue, 16 Oct 2018 11:50:41 GMT
    Etag: "e8c188bf776718c199710f6b27d2c203c81b3cda"
    Content-Length: 1245
    
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE Trans SYSTEM "trans-14.dtd">
    <Trans scribe="est-speech2txt" audio_filename="441c9090-0364-4a3d-9d40-1f66da92a4c2" version="1" version_date="181016">
    ...

If the transcription process failed (or the ID is invalid), the response will look like this:

    HTTP/1.1 404 Trancription for request 441c9090-0364-4a3d-9d40-1f66da92a4c5 not found
    Server: TornadoServer/4.5.3
    Content-Type: text/html; charset=UTF-8
    Date: Tue, 16 Oct 2018 11:52:02 GMT
    Content-Length: 0







