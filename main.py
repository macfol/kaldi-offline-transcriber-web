#!/usr/bin/env python3

import logging
import os
import uuid
import shlex
import subprocess
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.gen
import tornado.process
import tornado.options
import tornado.httpclient
from tornado.log import enable_pretty_logging

class Application(tornado.web.Application):
    def __init__(self):
        self.ROOT = os.path.dirname(os.path.abspath(__file__))
        path = lambda root,*a: os.path.join(root, *a)
        settings = dict(
            cookie_secret="43oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            static_path=path(self.ROOT, 'static'),
            xsrf_cookies=False,
            autoescape=None,
            serve_traceback=True
        )

        handlers = [
            (r"/upload", UploadHandler),
            (r"/result", ResultHandler)
        ]

        tornado.web.Application.__init__(self, handlers, **settings)
        self.running_requests = set()

async def start_transcribe(application, request_id, audio_filename, trs_filename, callback_url):
    application.running_requests.add(request_id)
    try:
        logging.info("Starting to transcribe file")        
        cmd = tornado.options.options.transcribe_command.format(audio=os.path.basename(audio_filename), trs=os.path.basename(trs_filename))
        tornado.process.Subprocess.initialize()
        logging.info("Running command: %s" % cmd)
        proc = tornado.process.Subprocess(shlex.split(cmd), stdout=tornado.process.Subprocess.STREAM, stderr=subprocess.STDOUT, cwd=application.ROOT, shell=False)
        output = (await proc.stdout.read_until_close()).decode("utf-8") 
        ret = await proc.wait_for_exit(raise_error=False)
        logging.info("Transcribing of request %s ended with status %d" % (request_id, ret))
        if callback_url:
            callback_url_with_id = callback_url + "?id=" + request_id
            logging.info("Making a callback request to " + callback_url_with_id)
            http_client = tornado.httpclient.AsyncHTTPClient()
            if ret == 0 and os.path.exists(trs_filename):        
                trs_body = open(trs_filename, "r").read()
                http_client.fetch(callback_url_with_id, method="PUT", body=trs_body, headers={"X-Status":"0", "X-Status-Reason":"OK", "Content-Type": "application/xml"})
            else:
                logging.warn("Transcribing failed. Log follows.\n" + str(output))
                http_client.fetch(callback_url_with_id, method="PUT", body="", headers={"X-Status":"1", "X-Status-Reason":"Transcribing failed"})
    finally:
        application.running_requests.remove(request_id)


class UploadHandler(tornado.web.RequestHandler):

    def initialize(self):
        self.request_id = str(uuid.uuid4())
        
    def put(self, *args, **kwargs):
        callback_url = self.get_argument("callback", None)
        file_extension = self.get_argument("extension")
        file_body = self.request.body
        audio_filename = tornado.options.options.user_file_dir + "/" + self.request_id + "." + file_extension
        trs_filename = tornado.options.options.user_file_dir + "/" + self.request_id + ".trs" 
        with open(audio_filename, "wb") as f:
            f.write(file_body)
        io_loop = tornado.ioloop.IOLoop.current()
        io_loop.spawn_callback(start_transcribe, self.application, self.request_id, audio_filename, trs_filename, callback_url)
        self.set_header('X-Request-Id', self.request_id)


class ResultHandler(tornado.web.RequestHandler):

    def get(self, *args, **kwargs):
        request_id = self.get_argument("id")
        if request_id in self.application.running_requests:
            self.set_status(204, reason="In progress")
        else:
            trs_filename = tornado.options.options.user_file_dir + "/" + request_id + ".trs"
            if os.path.exists(trs_filename):
                self.set_header("Content-Type", "application/xml")
                self.write(open(trs_filename).read())
            else:
                self.set_status(404, reason="Trancription for request %s not found" % request_id)


def main():
    enable_pretty_logging()

    logging.debug('Starting up server')

    tornado.options.define("port", default=8888, help="run on the given port", type=int)
    tornado.options.define("user_file_dir", default="./upload", help="Directory for storing audio and transcriptions files")
    tornado.options.define("transcribe_command", default="speech2text.sh {audio} {trs}", help="Command for transcribing an audio file", type=str)
    tornado.options.define("config", type=str, help="path to config file", callback=lambda path: tornado.options.parse_config_file(path, final=True))
    tornado.options.parse_command_line()
    tornado.options.options.user_file_dir = os.path.expanduser(tornado.options.options.user_file_dir)
    app = Application()
    logging.info('Listening on port %d' % tornado.options.options.port)
    app.listen(tornado.options.options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
